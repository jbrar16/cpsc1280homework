cd ~/environment/cpsc1280homework/assignment1

mkdir script1

mkdir script1/Pumpkin
mkdir script1/Diakon
mkdir script1/Carrot
mkdir script1/Parsenip


mkdir script1/Pumpkin/Kabocha
mkdir script1/Pumpkin/WinterMelon

mkdir script1/Diakon/Pickle
mkdir script1/Diakon/Oden

mkdir script1/Carrot/Orange
mkdir script1/Carrot/Oden

mkdir script1/Parsenip/Chips
mkdir script1/Parsenip/Soup

cp ~/environment/cpsc1280homework/Datasets/pg58.txt ~/environment/cpsc1280homework/assignment1/script1/Diakon/Pickle
cp ~/environment/cpsc1280homework/Datasets/pg972.txt ~/environment/cpsc1280homework/assignment1/script1/Carrot
cp ~/environment/cpsc1280homework/Datasets/pg972.txt ~/environment/cpsc1280homework/assignment1/script1/Carrot/Oden
cp ~/environment/cpsc1280homework/Datasets/heart.csv ~/environment/cpsc1280homework/assignment1/script1/Pumpkin
cp ~/environment/cpsc1280homework/Datasets/heart.csv ~/environment/cpsc1280homework/assignment1/script1/Parsenip/Chips

cd script1

chmod a=r Pumpkin
chmod a-rwx Diakon
chmod u+rwx Diakon
chmod a-rwx Carrot
chmod a=r Parsenip