
ls -R -l "$1" | sed -e '/^-/!d' | sort -r -k 2,2 | head -n10 > temp.txt
echo "Top 10 files with most hardlinks are :"
cat temp.txt
filename=$(head -n1 temp.txt | cut -d" " -f9)
path=$(find . -name "$filename" -type f -print)
inode=$(ls -i $path | cut -d" " -f1)
echo " "
echo "The file with same inode number as top in the list are:"
find -inum $inode
rm temp.txt