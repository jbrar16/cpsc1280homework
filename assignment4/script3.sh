find $1 -type f | cut -d"/" -f2-> d1
find $2 -type f | cut -d"/" -f2-> d2
comm -23 <(sort d1) <(sort d2) > $3
comm -13 <(sort d1) <(sort d2) > $4
comm -12 <(sort d1) <(sort d2) > $5