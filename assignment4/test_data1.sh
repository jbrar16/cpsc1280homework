cd ~/environment/cpsc1280homework/assignment4/
mkdir top
cd top
mkdir a b c

cd a
mkdir aa ab
touch aa/test1 aa/test2 aa/test3 aa/test4
touch ab/test5 ab/test6 ab/test7 ab/test8

cd ..
cd b
mkdir ba bb
touch ba/test9 ba/test10 ba/test11 ba/test12
touch bb/test13 bb/test14 bb/test15 bb/test16

cd ..
cd c
mkdir ca cb
touch ca/test17 ca/test18 ca/test19 ca/test20
touch cb/test21 cb/test22 cb/test23 cb/test24

cd ~/environment/cpsc1280homework/assignment4/

ln -f top/a/aa/test1 top/a/ab/test5
ln -f top/a/aa/test1 top/a/aa/test2

ln -f top/a/ab/test5 top/c/cb/test24
ln -f top/a/ab/test5 top/a/aa/test1
ln -f top/a/ab/test5 top/b/ba/test12

ln -f top/b/ba/test11 top/c/cb/test23
ln -f top/b/ba/test11 top/c/ca/test17

ln -f top/a/ab/test7 top/b/bb/test16
ln -f top/a/ab/test7 top/c/cb/test22

ln -f top/b/ba/test9 top/c/ca/test18
ln -f top/b/ba/test9 top/a/ab/test5
ln -f top/b/ba/test9 top/a/aa/test2