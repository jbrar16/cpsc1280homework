ipvOne="$1"
ipvTwo="$2"

ipvF1=$(echo $ipvOne | cut -d "." -f1 )
ipvF2=$(echo $ipvOne | cut -d "." -f2 )
ipvF3=$(echo $ipvOne | cut -d "." -f3 )
ipvF4=$(echo $ipvOne | cut -d "." -f4 )
ipvFSum=$(($((16777216*$ipvF1))+$((65536*$ipvF2))+$((256*$ipvF3))+$ipvF4))

ipvS1=$(echo $ipvTwo | cut -d "." -f 1 )
ipvS2=$(echo $ipvTwo | cut -d "." -f 2 )
ipvS3=$(echo $ipvTwo | cut -d "." -f 3 )
ipvS4=$(echo $ipvTwo | cut -d "." -f 4 )
ipvSSum=$(($((16777216*$ipvS1))+$((65536*$ipvS2))+$((256*$ipvS3))+$ipvS4))

pCount=$(($ipvSSum-$ipvFSum))
declare -i count=0
declare -i ipvR1=247
declare -i ipvR2=255
declare -i ipvR3=255
declare -i ipvR4=255

if [ $ipvFSum -ge $ipvSSum ]
    then
        >&2 echo "1"
    else
        echo "ipAddress , avgTime , ttl , failedAttempts , successfulAttempts , initiatingTime" >> pingresult.csv
        
        while [ $count -le $pCount ]
        do
            validServer=$(sudo ping -c 10 -w 1 "$ipvF1.$ipvF2.$ipvF3.$ipvF4" | grep ", 1 received," | cut -d "," -f 2)
            if test -z "$validServer"
                then
                    echo "$ipvF1.$ipvF2.$ipvF3.$ipvF4:Not Reachable" >> pingresult.csv
                else
                    validIp=$(sudo ping -c 10 "$ipvF1.$ipvF2.$ipvF3.$ipvF4")
                    avgTime=$(echo $validIp | cut -f7 -d "-" | cut -f2 -d "=" | cut -f2 -d "/")
                    ttl=$(echo $validIp | cut -f2 -d ":" | cut -f3 -d " " | cut -f2 -d "=" )
                    pTmtd=$(echo $validIp | cut -f7 -d "-" | cut -f2 -d " ")
                    pRcvd=$(echo $validIp | cut -f7 -d "-" | cut -f5 -d " ")
                    failedAttempts=$(($pTmtd-$pRcvd))
                    iniTime=$(echo $validIp | cut -f2 -d ":" | cut -f4 -d " " | cut -f2 -d "=" )
                    echo "$ipvF1.$ipvF2.$ipvF3.$ipvF4 , $avgTime , $ttl , $failedAttempts , $pRcvd , $iniTime" >> pingresult.csv
            fi
            
            ipvF4=$(($ipvF4+1))
            count=$(($count+1))
            
            if [ $ipvF4 -gt $ipvR4 ]
                then
                    ipvF4=$(($ipvF4-$ipvF4))
                    ipvF3=$(($ipvF3+1))
            fi
            
            if [ $ipvF3 -gt $ipvR3 ]
                then
                    ipvF3=$(($ipvF3-$ipvF3))
                    ipvF2=$(($ipvF2+1))
            fi
            
            if [ $ipvF2 -gt $ipvR2 ]
                then
                    ipvF2=$(($ipvF2-$ipvF2))
                    ipvF1=$(($ipvF1+1))
            fi
            
            if [ $ipvF1 -gt $ipvR1 ]
                then
                    count=$(($count+$pCount))
            fi
        
        done
        
fi