# cpsc1280homework

# links
[Git reference page](https://git-scm.com/docs)

[AWS educate page](https://aws.amazon.com/education/awseducate/)

# Start here:
Fork this project for your CPSC 1280 homework.
Submit **all** your homework via your repository.

# Basic commands:
## Make a local copy of your repository
git clone `URL here`

## Staging files. 
### You need to stage files before committing them
git add *

git add `file pattern`

## Committing files
git commit -m"`Your message here`"

## Push changes to your cloud repository
git push

## Getting changes from your cloud repsoitory
git pull



Acknowledge that you have read the file by adding **your name**:

Name: Jobanpreet Brar
